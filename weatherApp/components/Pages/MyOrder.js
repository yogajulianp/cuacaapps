import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  Button,
} from "react-native";
import { Data } from "./data";

export default function MyOrder() {

  return (
    <View style={styles.container}>
      <View style={styles.TopText}>
        <Image
          style={styles.logoDrawwer}
          source={require("./images/Logo.png")}
        />
        <Text>Rincian Produk</Text>
      </View>

      <View style={styles.Cover}></View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "flex-start",
          padding: 16,
        }}
      >

        <View style={styles.cover}>
          <Text>Rincian Produk</Text>
          <Image
          style={styles.coverHome}
          source={require("./images/cover-home.png")}
        />
        </View>
      </View>

      <SafeAreaView style={styles.homebox}>
        <FlatList
          data={Data}
          keyExtractor={(item) => item.id}
          numColumns={2}
          renderItem={({ item }) => {
            return (
              <View style={styles.content}>
                <View style={styles.boxProduct}>
                  <View style={styles.isiBox}>
                    <Text>{item.title}</Text>
                    <Image
                      style={{ height: 100, width: 100, marginRight: 5 }}
                      source={item.image}
                    />
                    <Text>{`Rp ${item.harga}`}</Text>
                    <Text>{item.desc}</Text>
                    <TouchableOpacity>
                      <View style={styles.orderButton}>
                     
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>

                <View
                  style={{ borderBottomWidth: 1, borderBottomColor: "#A8AAAB" }}
                />
              </View>
            );
          }}
        />
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    },
  TopText: {
    flex: 1,
    paddingTop: 60,
    paddingBottom: 10,
    alignItems: "flex-end",
    height: 60,
    flexDirection: "row",
    justifyContent: "space-between",
    flexDirection: "row",
  },
  logoDrawwer: {
    height: 60,
    width: 120,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
    paddingTop: 40,
  },
  cover: {
    flex: 1,
  },
  coverHome:{
    height: 120,
    width: "100%",
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
    paddingTop: 10,
  },
  homebox: {
    alignItems: "center",
    justifyContent: "space-around",
    flexDirection: "row",
    paddingTop: 20,
    paddingHorizontal: 20,
    paddingBottom: 100,
    backgroundColor: "#EFEFEF",
  },
  content: {
    width: 150,
    height: 250,
    margin: 5,
    borderWidth: 1,
    alignItems: "center",
    borderRadius: 5,
    borderColor: "grey",
    paddingBottom: 40,
  },
  boxProduct: {
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 20,
    paddingBottom: 20,
    paddingTop: 10,
    
  },

  orderButton: {
    alignItems: "center",
    justifyContent: "center",
  },
  isiBox: {
    paddingTop: 40,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: 'center',
  },
});
