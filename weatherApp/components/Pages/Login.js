import React, { useEffect, useState } from "react";
import { Alert } from 'react-native';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Button,
} from "react-native";
import firebase from "firebase/app";
import 'firebase/auth';

import { Entypo } from "@expo/vector-icons";

export default function Login ({ navigation })  {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");



// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDEZHBp-KTNKsoVsMO0RrrVYD-Jo5NGwiE",
  authDomain: "marketplace-finaljcc.firebaseapp.com",
  projectId: "marketplace-finaljcc",
  storageBucket: "marketplace-finaljcc.appspot.com",
  messagingSenderId: "165789269316",
  appId: "1:165789269316:web:4825e6bfbda25c582ceb11"
};


// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

  const Submit = () => {
    const account = {
      email,
      password,
    };
    console.log(account);

    firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then((userCredential) => {
      // Signed in 
      var user = userCredential.user;
      
      navigation.navigate("MarketPlace")
      //console.log("User Berhasil Login")
      alert('Login Berhasil')
      // ...
    })
    .catch((error) => {
      var errorCode = error.code;
      var errorMessage = error.message;
      // ..
      Alert.alert("Login Gagal, Harap daftar dahulu", errorMessage)
      //console.log("User Gagal Login")
      //navigation.navigate("Registrasi")
    });

  };

  return (
    <View style={styles.container}>
      <View style={styles.title}>
        <View>
          <Text style={styles.titleText}>Login</Text>
        </View>
        <View>
          <Image
            style={styles.logoDrawwer}
            source={require("./images/Logo.png")}
          />
        </View>
      </View>



      <View style={styles.formContainer}>
        <Text style={styles.formAccount}>Email</Text>
        <View style={styles.formFieldContainer}>
          <TextInput
            placeholder="Email"
            style={styles.formFieldInput}
            value={email}
            onChangeText={(value) => setEmail(value)}
          />
        </View>
      </View>

      <View style={styles.formContainer}>
        <Text style={styles.formAccount}>Password</Text>
        <View style={styles.formFieldContainer}>
          <TextInput
            placeholder="Password"
            style={styles.formFieldInput}
            value={password}
            onChangeText={(value) => setPassword(value)}
          />
          <Entypo name="eye" size={24} color="grey" />
        </View>
      </View>

  

      <View style={styles.footer}>
        <Button onPress={Submit}
         title="Login" style={styles.button} />
        <View style={{ padding: 10 }}></View>
        <Button
          style={styles.button}
          title="Daftar"
          variant="outlined"
          onPress={() => navigation.navigate("Register")}
        />
      </View>
      <View style={styles.lineContainer}></View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 35,
    backgroundColor: "#48EBEB",
  },
  header: {
    flex: 1,
    justifyContent: "center",
  },
  headerText: {
    fontSize: 35,
    alignSelf: "center",
    fontWeight: "bold",
  },
  title: {
    flex: 1,
    paddingTop: 30,
    paddingBottom: 10,
    alignItems: "flex-end",
    height: 60,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  titleText: {
    fontSize: 24,
    fontWeight: "bold",
  },
  form: {
    flex: 6,
  },
  formContainer: {
    marginBottom: 18,
  },
  formAccount: {
    fontSize: 14,
    marginBottom: 4,
    fontWeight: "bold",
  },
  formFieldContainer: {
    borderWidth: 1,
    borderColor: "grey",
    borderRadius: 11,
    padding: 6,
    flexDirection: "row",
  },
  formFieldInput: {
    fontSize: 14,
    fontWeight: "bold",
    flex: 1,
  },
  footer: {
    marginVertical: 20,
    alignItems: "stretch",
  },
  button: {
    flex: 1,
    padding: 10,
    backgroundColor: "#003366",
    alignItems: "center",
    margin: 5,
    borderRadius: 12,
  },
  buttonText: {
    fontWeight: "bold",
    width: "100",
    textAlign: "center",
  },
  logoDrawwer: {
    height: 40,
    width: 100,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
    paddingTop: 5,
  },
  footerText: {
    fontSize: 14,
    justifyContent: "center",
    alignSelf: "center",
    fontWeight: "bold",
  },
  lineContainer: {
    flex: 1,
  },
  formText: {
    fontSize: 8,
  },
});
