import axios from 'axios';
import React, { useState, useEffect } from 'react';
import {Button, FlatList, View, SafeAreaView, Text, Image, StyleSheet, TextInput, TouchableOpacity,} from "react-native";

export default function RestApi() {
    const [title, setTitle]=useState("");
    const [value, setValue]=useState("");
    const [items, setItems]=useState([]);
    const [button, setButton]=useState("Simpan");
    const [selectedUser, setSelectedUser] = useState({});

    const submit =()=> {
        const data ={
            value, title
        }
        console.log("data before send : ", data)
        if (button == "Simpan") {
            axios.post(`https://achmadhilmy-sanbercode.my.id/api/v1/news`, data)
            .then(res=>{
                console.log('res: ', res)
                setTitle("")
                setValue("")
                GetData()
            }).catch(err=>{
                console.log('error: ', err)
            })
        }else{
            axios.put(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${selectedUser.id}`, data)
            .then(res=>{
                console.log('res: ', res)
                setTitle("")
                setValue("")
                GetData()
                setButton("Simpan")
            }).catch(err=>{
                console.log('error: ', err)
            });
        }
    };

    const onSelectItem =(item)=>{
        console.log(item)
        setSelectedUser(item)
        setTitle(item.title)
        setValue(item.value)
        setButton("Update")
    }

    const GetData=()=> {
        axios.get('https://achmadhilmy-sanbercode.my.id/api/v1/news')
        .then(res=>{
            const data1=(res.data.data)
            console.log('res: ', data1)
            setItems(data1)
        })
    }
    const onDelete=(item)=> {
        axios.delete(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${item.id}`)
        .then(res=>{
            console.log('res: ', res)
            GetData()
        }).catch(err=>{
            console.log('error: ', err)
        })
    }

    useEffect(() => {
        GetData()
    },[])

    return(
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.title}>Tulis Belanja yang akan dipesan </Text>
            </View>
            <View style={styles.content1}>
                <Text>Katagori</Text>

                <TextInput
                    placeholder="Masukan katagori"
                    style={styles.input}
                    value={title}
                    onChangeText={(value)=>setTitle(value)}
                />
                <Text>Nama Barang Belanja</Text>
                <TextInput
                    placeholder="Masukan barang belanjaan"
                    style={styles.input}
                    value={value}
                    onChangeText={(value)=>setValue(value)}
                />
            <Button
            title={button}
            onPress={submit}
            />
            </View>

            <View style={styles.content1}>
                <Text>Barang yang akan dibelanjakan</Text>

                <FlatList
                    data={items}
                    keyExtractor={(item, index)=> `${item.id}-${index}`}
                    renderItem={({item})=> {
                    return(
                        <View>
                            <TouchableOpacity onPress={() =>onDelete(item)}>
                                <Text style={{color:'red', alignSelf:'flex-end'}}>Delete</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=>onSelectItem(item)}style={{borderRadius: 6,backgroundColor:'grey', padding: 5, marginBottom:10}}>
                            <Text style={{color:'yellow'}}>{`Kategori            : ${item.title}`}</Text>
                            <Text style={{color:'white'}}>{`Nama barang  : ${item.value}`}</Text>
                            </TouchableOpacity>
                        </View>
                        )
                    }}
                />



            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
    },
    header: {
        paddingTop: 20,
        paddingHorizontal: 16,
        backgroundColor: '#699BF7',
        alignItems: 'center'
    },
    title: {
        color: 'white',
        fontSize: 20,
    },
    content1: {
        paddingHorizontal: 16
    },
    input: {
        borderWidth: 1,
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderRadius: 6,
        marginBottom: 10
    },
    contentNews: {
        backgroundColor: 'grey',
        paddingVertical: 10
    }
})