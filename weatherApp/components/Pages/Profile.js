import { NavigationContainer } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import { Alert } from 'react-native';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TextInput,
    SafeAreaView,
    TouchableOpacity,
    FlatList,
    Button,
  } from "react-native";
import firebase from "firebase/app";
import "firebase/auth";

export default function Profile({ navigation }) {
  const Logout =()=>{
    firebase.auth().signOut()
    .then(()=>{
        console.log("User Berhasil Logout")
        navigation.navigate("Login")
    });
  }

  return (
    
      <View style={styles.container}>
          <View style={styles.topLine}>
          <Button onPress={Logout} title="Logout"/>
          <Image
            style={{height: 40, width: 100,marginHorizontal:15}}
            source={require("./images/avatarAkun.png")}
          />
          <Image
            style={{height: 20, width: 20,marginRight: 8}}
            source={require("./assets/Lonceng.png")}
          />
          <Image
            style={{height: 20, width: 20,marginRight: 8}}
            source={require("./assets/Pesan.png")}
          />
          <Image
            style={{height: 20, width: 20,marginRight: 8}}
            source={require("./assets/Setting.png")}
          />

          </View>



        <View style={styles.header}>
          <Text style={styles.headerText}>My Account</Text>
          <Image
            style={{
              height: 40,
              width: 40,
              marginRight: 5,
              alignSelf: "center",
            }}
            source={require("./images/userProfile.png")}
          />
          <Text style={styles.titleText}>Yoga Julian Prasutiyo</Text>
          <Text style={{color:"grey", alignSelf: "center"}}>Silver Member</Text>
        </View>

        <View style={styles.form}>
          <View style={styles.dataContainer}>
            <Text style={styles.formAccount}>Transaksi</Text>
          <View style={styles.formFieldContainer}>

            <View style={styles.biodata}>
              <View style={styles.icon}>
                <Image
                  style={{ height: 30, width: 30, marginRight: 5 }}
                  source={require("./assets/credit-card.png")}
                />
                <Text style={{fontSize: 10}}>Tagihan</Text>
              </View>

              <View style={styles.icon}>
                <Image
                  style={{ height: 30, width: 30, marginRight: 5 }}
                  source={require("./assets/paper-plane.png")}
                />
                <Text style={{fontSize: 10}}>Diproses</Text>
              </View>

              <View style={styles.icon}>
                <Image
                  style={{ height: 30, width: 30, marginRight: 5 }}
                  source={require("./assets/inbox.png")}
                />
                <Text style={{fontSize: 10}}>Dikemas</Text>
              </View>

              <View style={styles.icon}>
                <Image
                  style={{ height: 40, width: 40, marginRight: 5 }}
                  source={require("./assets/car.png")}
                />
                <Text style={{fontSize: 10}}>Dikirim</Text>
              </View>

              <View style={styles.icon}>
                <Image
                  style={{ height: 40, width: 40, marginRight: 5 }}
                  source={require("./assets/award.png")}
                />
                <Text style={{fontSize: 10}}>Ulasan</Text>
              </View>

              </View>

            </View>
          </View>

          <View style={styles.dataContainer}>
            <Text style={styles.formAccount}>MEDIA SOSIAL</Text>
            <View style={styles.formFieldContainer}>

            <View style={styles.biodata}>
              <View style={styles.icon}>
                <Image
                  style={{ height: 40, width: 40, marginRight: 5 }}
                  source={require("./assets/instagram.png")}
                />
                <Text style={{fontSize: 10}}>@MarketPlace</Text>
              </View>

              <View style={styles.icon}>
                <Image
                  style={{ height: 40, width: 40, marginRight: 5 }}
                  source={require("./assets/Twitter.png")}
                />
                <Text style={{fontSize: 10}}>@MarketPlace</Text>
              </View>

              <View style={styles.icon}>
                <Image
                  style={{ height: 30, width: 30, marginRight: 5 }}
                  source={require("./assets/facebook.png")}
                />
                <Text style={{fontSize: 10}}>@MarketPlace</Text>
              </View>
              </View>
            </View>
          </View>

        </View>
        <View style={{padding: 60, flex:2,}}></View>
      </View>
    
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 35,
    backgroundColor:"white",
  },
  topLine: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    borderRadius: 5,
    backgroundColor: "#48EBEB",
    alignItems: "center",
    height: 60,
  },
  header: {
    justifyContent: "center",
    borderRadius: 45 / 2,
  },
  headerText: {
    fontSize: 25,
    alignSelf: "center",
    fontWeight: "bold",
    paddingTop: 50,
  },
  title: {
    flex: 1,
    justifyContent: "center",
  },
  titleText: {
    fontSize: 20,
    fontWeight: "bold",
    alignSelf: "center",
    opacity: 0.8,
  },
  form: {
    flex: 3,
    borderRadius: 12,
  },
  dataContainer: {
    margin: 18,
    backgroundColor: "#EFEFEF",
    paddingTop: 20,
    paddingBottom: 20,
  },
  formAccount: {
    fontSize: 14,
    marginBottom: 4,
    fontWeight: "bold",
  },
  formFieldContainer: {
    borderWidth: 1,
    borderColor: "grey",
    borderRadius: 11,
    padding: 1,
    backgroundColor: "white",
  },
  formFieldInput: {
    fontSize: 14,
    fontWeight: "bold",
    flex: 1,
  },
  biodata: {
    flexDirection: "row",
    alignSelf: "center",
    justifyContent: 'space-around',
    alignItems: 'center',
    fontSize: 10,
  },
  icon: {
    alignItems: 'center',
    alignSelf: "center",
    padding: 5,
    justifyContent: 'center',
    alignContent: 'center',
  
  }
});


