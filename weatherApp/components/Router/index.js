import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Ionicons } from '@expo/vector-icons';


import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


import Invoice from '../Pages/Invoice';
import MyOrder from '../Pages/MyOrder';
import Home from '../Pages/Home';
import Login from '../Pages/Login';
import Profile from '../Pages/Profile';
import Register from '../Pages/Register';


const Tab = createBottomTabNavigator();
const Drawwer = createDrawerNavigator();
const Stack = createNativeStackNavigator();


export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Login" component={Login}  options={{headerShown: false}}/>
                <Stack.Screen name="Home" component={Home}/>
                <Stack.Screen name="My Journey" component={MainApp}/>
                <Stack.Screen name="MarketPlace" component={MyDrawwer} />
                <Stack.Screen name="Register" component={Register}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
};


const MainApp =()=>(
        <Tab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;
  
              if (route.name === 'Home') {
                iconName = 'home';
              } else if (route.name === 'Detail') {
                iconName =  'book';
              } else if (route.name === 'Daftar Belanja') {
                iconName =  'ios-basket-sharp';
              } else if (route.name === 'Profil') {
                iconName = 'ios-person-circle-sharp';
              }
  
              // You can return any component that you like here!
              return <Ionicons name={iconName} size={size} color={color} />;
    
            },
            tabBarActiveTintColor: '#699BF7',
            tabBarInactiveTintColor: 'gray',
          })}
        >   
           <Tab.Screen name="Home" component={Home} options={{headerShown: false}}/>
           <Tab.Screen name="Detail" component={MyOrder} options={{headerShown: false}}/>
           <Tab.Screen name="Daftar Belanja" component={Invoice} options={{headerShown: false}}/>
           <Tab.Screen name="Profil" component={Profile} options={{headerShown: false}}/>
        </Tab.Navigator>
);

const MyDrawwer =()=>(
    <Drawwer.Navigator>
        <Drawwer.Screen name="My Journey" component={MainApp} options={{headerShown:false}}/>
        <Drawwer.Screen name="Daftar Belanja" component={Invoice} options={{headerShown: false}}/>
    </Drawwer.Navigator>
);
